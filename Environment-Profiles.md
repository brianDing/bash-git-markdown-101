# Environment and Profiles

*What is an environment?*
- Where code runs. Could be your local, dev, testing or production machine.


*What is an environment variable?*
- Variable that machine has access to in that specific environment.
- Explicitly set for specific purpose.
- Useful information for the terminal, machine and user or sensitive information such as API keys that identify a user/system.
- Used to set IP of DB or another service. 

---
## Profiles
*.bash_profile*
- Read after login after initial environment is established with ENV variables and runs the profile for the shell (i.e. bash uses bash_profile)
- Uses export keyword to establish global variables

*.bashrc*
- Open a shell to run processes will run things inside this file
- Add features to process memory rather than environment memory
- Depends on whether the command is interactive (i.e. type commands on CLI) or requires the .bashrc 
- If its not interactive (i.e. just a script) then .bashrc will not be loaded into the process memory

*On login a shell is started 

~/file = root directory/file

Child processes inherit parent variables 

Kill a parent process to a child will create an orphan which is then attached to process 1 (i.e. the unix system) to listen to the exit process

---

## What is PATH?
- Files any shell/ terminal reads before opening.
### Usually sets:
- What programs does it know?
- What aliases do I know?
- What variables do I know?
- It can set aesthetics and Environment variables.
- Things like Brew or Atom have the path to the executable declared on PATH.
- By typing brew or atom in the cli it will call the path to find where the app is and run the executable
- Look through your $PATH to find absolute path to your command

---

## Variables
*Defining a variable*
```bash
$VAR_NAME=VALUE
```

*Print a variable*
```bash 
$echo $VAR_NAME
```


*How to find environment variables?*
```bash
$env
```

*How to print environment variables?*
```bash
$echo $<variable_name>
```


*Notes*
- To ensure a variable is reachable in child processes i.e running a script or file, you need to export the variable*
- Once a variable is exported, the variable will persist for the current process and all its child processes
- Environment variables are established before login or creation of a shell
- Variables that are not set in the environment are not remembered as exporting a variable puts it in memory but is wiped on logging out or leaving the session
- Environment variables are “permanent” as they are established on login every time before anything is ran
- By adding variables into the profile file, you need to run "source .bash_profile" or the profile for the shell, it will reload the profile to load the new variables into the environment 
- Source basically loads in the file into the current process

---
## Permissions 
- Check -> ls -al
- Example:
```
drwxrwxr-x 5 brian brian 4096 Apr  9 08:44 movies
```
- Structure of 10 letters 
  - First letter defines file type
  - Every three letters represent a group (User, Group, Other)
  - r = read
  - w = write
  - x = execute 
- Number of links
- User that the permissions apply to
- Group the second block of permissions apply to
  

*HOW SECURITY WORKS ON UNIX / LINUX*
- If you own the file you are known as the user
- If you are part of a group i.e. share permissions under a specific group
- If you own the file and part of a group, the permission to the file is based on what is given the user overwriting the group
- If you do not own the file but are part of the group, you have the permissions to the file shared across the group 
- If you are neither the owner or part of the group, you have permissions defined by the last 3 


*How to change permissions*
- If you own the file, you can run chmod to change permissions
```bash
    $chmod u+x files
```
- u = user, +x = add executable permission, file name
- chmod can be used to change user, group or other permissions
- To add permission(+), take away permission(-) or set (=)
- Type of permission (r= read, w=write, execute=x)

*Alternative to chmod syntax is using Octal (number) notation*
- READ = 4, WRITE = 2, EXECUTE = 1 (SUM OF 7)
```bash
$chmod 700 file
```
- chmod is followed by 3 numbers, one for each user group (user, group, other)
- NEVER USE chmod 777