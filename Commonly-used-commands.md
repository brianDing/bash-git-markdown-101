# Questions
Make sure you are logged in as the vagrant user
- How can you check without logging out?
```bash
$id
```

Make sure you are in the home directory for the vagrant user
- How can you check?
```bash
$pwd
```

- If you are not there how could you get there quickly?
```bash
$cd
```

List all the files in your current directory including hidden ones
```bash
$ls -A
```
---
## Part 2

- Create a file called hello_world
- Create a file called .goodbye_world
- How can you see both files using ls?
```bash
$ls -f
```

Create the following directory structure in one command;
- books
- books/non-fiction
- books/fiction
- movies
- movies/action
- movies/drama
- movies/comedy/films
- movies/comedy/standup
- music
```bash
$mkdir books books/non-fiction books/fiction movies movies/action movies/drama movies/comedy movies/comedy/films movies/comedy/standup music
```

Copy the /etc/passwd file into movies/comedy/films
```bash
$cp /etc/passwd movies/comedy/films
```

Find out where the passwd command is and copy that file into movies/comedy/standup
```bash
$cp $(which passwd) movies/comedy/standup
```

Change directory to music and whilst in this directory rename your .goodbye_world so that it is in this directory instead, and no longer in your home directory.
```bash
$mv ../.good_world . 
```

Show how much disk space is being used by books, movies and music
```bash
$du books movies music
```

In one command find out what types of file the following are;
- The passwd command
- /etc/passwd
- /etc/hosts
- /usr/include/python2.7/pyport.h
- /etc/rc3.d
```bash
$file $(which passwd) /etc/passwd /etc/hosts /usr/include/python2.7/pyport.h /etc/rc3.d
```

Locate all the files in the /usr/bin and the /etc directory that begin with a p.
```bash
$find /usr/bin/p* /etc/p*
```
Find files in the operating system that have a modification date less than a day old
```bash
$find / -mtime +1 2>/dev/null
```

Find files in the operating system that have a suffix of .d
```bash
$find / -name '*.d' 2>/dev/null
```

Remove the books/fiction directory using rmdir
```bash
$rmdir books/fiction
```

What happens if you try using rmdir against movies/comedy/films?
- Cannot use commmand as there are contents in there, rmdir is for empty files

Remove the movies/comedy directory using one command
```bash
$rm -rf movies/comedy
```

Try removing the /etc/passwd file without changing user in any way
Unsure what that means

---
## Answering Steve's question 3
*What is the only character that cannot be used in a Linux/Unix file name*
- /

*Give me 2 ways in which I can create a file name containing a space*
- touch "hello world"
- touch hello\ world

*How could I create a file called -r and how could I remove it?*
- By adding a file path infront of it -> touch ./-r 
- Remove rm ./-r


---
## Extra exercises for Piping

1. Put a message in a file and then write the message to yourself with the write command.
```bash
$echo "hello world" > hello_world
$cat hello_world | write brian
```

2. Create 3 files in your home directory called file1, file2, file3. Use the cat command with redirection to create these files.
In these files put the text;
This is filen
```bash
$echo "this is file1" > touch file1
$echo "this is file2" > touch file2
$echo "this is file3" > touch file3
```

What happens when you do the following;
cat < file*

3. Using the files created in Q2, merge the contents into one file called filex.

4. Append to the end of filex the listing of your home directory in long format.

5. Using the find command search for all files that don’t have an owner or files that are bigger than 100 MB in size or have an owner called rpcuser. Disregard all errors, but store the information in a file called myfind.out.

