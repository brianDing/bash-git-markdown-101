# Linux Basics


- *In shared directory (e.g. /tmp) depending on the permissions you might not be able to have files with the same name*
- *To find out who is logged on run the who command*


## SHELL COMMANDS
---
- Help you write a command
- ENTER -> Shell does real work 
- Parse command line entered based on spaces 
- Shell takes input from stdin which default is the keyboard, spits out output to stdout and errors to stderr


## Shell directives (Things shell has to do before executing command)
---
- Directives are ran first before the command 
- Directives are expanded before executing the command listed 

*Wildcards (*, [ ], ?)*
```
/* is all files

/*a, end with a

/a*, start with a

/*a* contains a

[aeiou]* contains aeiou at the start, conditions not separated by anything 

????, files with 4 characters
```

---
*Subshell is asking shell to run another command, get output and use that output as another command*

```bash
echo $`ls` 
or
echo $(ls)
```
---
*Redirection (>, >>, 2>, 2>>, <)*

- '>' or '>>' takes output from stdout and write to file following symbol
    ```bash
    $echo 'hello world' > testfile (Overwrite)
    or 
    $echo 'hello world' >> testfile (Append to testfile)
    ```

- 2> or 2>> takes output from stderr and write to file following symbol
    ```bash
    $ls -A 2> /dev/null (Shares convention as above for )
    ```

- < X take input from X to be used in the command 
    ```bash
    $echo < cat testfile
    ```

---

Pipelines 
- (e.g. cmd1 | cmd2) Use output of command1 to command2

---

Variables (e.g. $DRINK)
  - Rewrites the command line to substitute the variable with its value
  - Searches for where your command lives
  - Refer to Environment-Profiles.md for more details about environment variables

---
## TLDR
```bash
$ls -d /etc/p*
```
- List files in /etc starting with p showing directories as themselves not their content
- /etc/p* is expanded first as they are the directive by the shell, then the command ls -d is ran to it
- Wildcards can be used on any command that takes a file/folder as an argument

---

## Processes
```bash
List out processes 
$ps -lf
```
- I Node -> Unique number for File or folder
- ProcessID -> Unique number for process 
- ParentProcessID -> Unique number for parent process 
- Everything in Linux is a tree structure with a PID (process ID) for itself and a PPID for its parent 
- The first process is the environment and forks out its other processes
- Each child process inherit parent process 
- Parent processes track child processes to receive exit codes when they quit


```bash
$sudo netstat -tlnp
```
- Check what net processes are running on what port