# Bash and Markdown 101
This repo is our documentation on bash and markdown. We will put here the main commands of bash and document it in markdown!

We will learn:
- Bash
- markdown

First we will learn:
1. bash
2. markdown 
---
## Bash
Bash is a language that speaks with the kernel. Used throughout Linux machines (90%)

### **Main commands**
#### Where am I?
    $cd
    > /user/path/location

#### Go somewhere.
```bash
cd directory
```

#### Go back.
```bash
cd ..
```

#### Create file
```bash
$touch file.txt
```

#### Create folder/ directory
``` bash
$mkdir folder
```

#### Remove file
```bash
$rm target
```

#### Remove directory
```bash
$rmdir pathToDirectory
or
$rm -rf pathToDirectory
```

#### Print to console 
```bash
$echo 'hello world'
> hello world
```

#### Truncate into file (Wipe content and then write)
```bash
$echo 'hello world' > example.txt
$cat example.txt
> hello world
```

#### Append into file (Wipe content and then write)
```bash
$echo 'hello world 2' >> example.txt
$cat example.txt
> hello world
> hello world 2
```