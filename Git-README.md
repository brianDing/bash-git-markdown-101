# Documenting GIT and BitBucket

## Steps
1. git init to create a new repository
2. git add <files> | git add . to track files
3. git commit -m "message" to commit to branch
4. git push to finally push onto branch 


## Extra commands
- git status to get status of files being tracked
- git logs to check number of commits and other info
- git checkout -b name to create new branch and checkout into it
- git checkout branch to checkout into an existing branch